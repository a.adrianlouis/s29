// ===================== 1 =============
/*
Find users with letter s in their firstName or d in their lastName.
	a. Use the $or operator.
	b. Show only the firstName and lastName fields and hide the _id field.
*/


db.users.find({
	$or:[
	{firstName:{
		$regex: "S"}
	},
	{lastName:{
		$regex: "D"}
	}
	
	]
	},
	{
		_id: 0,
		firstName: 1,
		lastName: 1
	}
);



// ===================== 2 =============
/*
Find users who are from HR department and their age is greater than or equal to 70.
	a. Use the $and operator
*/

db.users.find({
	$and:[
		{department:"HR"},
		{age:{
			$gte: 70
		}}
	]
});



// ===================== 3 =============
/*
Find users with the letter e in their firstName and has an age of less than or equal to 30.
	a. Use the $and, $regex and $lte operators
*/

db.users.find({
	$and:[
		{firstName:{
			$regex: "e"
		}},
		{age:{
			$lte: 30
		}}
		
	]
});